var width = 850;
var height = 400;

var changed = false;

function randomPoint(i)
{
    var newData = Math.floor(Math.random() * 100);
    var ranDead = Math.random() - 0.5;
    newIsDead = ranDead > 0 ? true : false;
    return {data:newData, index:i, sorted:false};
}

function getIndex(items, index)
{
    for (var k = 0; k < dataset.length; k++) {
        if (dataset[k].index == index)
            var iElement = k;
    }

    return items[iElement];
}

function setSorted(dataset, j)
{
    getIndex(dataset, j).sorted = true;
}



function quickSort(dataset, circles, xScale, yScale)
{
    var waitTime = 0; // for delaying the animation

    for (var i = 0; i < dataset.length - 1; i++) {
        for (var j = 0; j < dataset.length - i - 1; j++) {
            waitTime = 0;
            for (var k = 0; k < i; k++)
                waitTime += dataset.length - k - 1;
            waitTime += j;



            setTimeout(swap, 850 * waitTime, dataset, circles, j, j+1, xScale, yScale);

        }

        setTimeout(setSorted, 850 * waitTime, dataset, dataset.length - i - 1);
    }

    setTimeout (setSorted, 850 * waitTime + 1, dataset, 0);
    setTimeout(swap, 850* waitTime + 1, dataset, circles, 0, 0, xScale, yScale);
}

function swap(dataset, circles, i, j, xScale, yScale)
{
    for (var k = 0; k < dataset.length; k++) {
        if (dataset[k].index == i)
            var iElement = k;
    }

    for (var k = 0; k < dataset.length; k++) {
        if (dataset[k].index == j)
            var jElement = k;
    }

    if (dataset[iElement].data > dataset[jElement].data) {
        var temp = dataset[iElement].index;
        dataset[iElement].index = dataset[jElement].index;
        dataset[jElement].index = temp;
        changed = true;
    }

    circles
        .data(dataset)
        .transition().duration(150)
        .attr("r", function (d) { if (d.index == i || d.index == j) return 15;
                else return 10; } )
        .style("fill", function (d) { if (d.sorted) return "#3e6";
                if (d.index == i || d.index == j) return "#f33";
                else return "#36e"; });

    circles
        .data(dataset)
        .transition().duration(250).delay(200)
        .attr("cx", function (d) { return xScale(d.index); });

    circles
        .data(dataset)
        .transition().duration(150).delay(600)
        .attr("r", 10)
        .style("fill", function (d) { if (d.sorted) return "#3e6";
                else return "#36e";});


    console.log(i, ", ", j);
}

function randomize(dataset, circles, xScale, yScale)
{
    dataset = [];

    for (var i = 0; i < 10; i++)
            dataset.push(randomPoint(i));

        canvas = d3.select("#canvas").append("svg")
            .attr("width", width)
            .attr("height", height)

        circles = canvas.selectAll("circle")
            .data(dataset)
            .enter()
            .append("circle")
            .attr("cx", function (d) { return xScale(d.index); })
            .attr("cy", function (d) { return height - yScale(d.data); })
            .attr("r", 10)
            .style("fill", "#36e");

}

var dataset = [];

for (var i = 0; i < 10; i++)
        dataset.push(randomPoint(i));

var xScale = d3.scale.linear();
xScale.domain([0, dataset.length - 1]);
xScale.range([40, 800]);

var yScale = d3.scale.linear();
yScale.domain([0, 100]);
yScale.range([10, 390]);

    canvas = d3.select("#canvas").append("svg")
        .attr("width", width)
        .attr("height", height)

    circles = canvas.selectAll("circle")
        .data(dataset)
        .enter()
        .append("circle")
        .attr("cx", function (d) { return xScale(d.index); })
        .attr("cy", function (d) { return height - yScale(d.data); })
        .attr("r", 10)
        .style("fill", "#36e");


var circles;

quickSort(dataset, circles, xScale, yScale);
